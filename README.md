## Arduino Apps from Simulink Realtime Target

- test01.slx  
   First tests, external mode and connected io

- test02.slx
    Digital IO, PWM and DAC

- test03.slx
    Empty control loop at Arduino, template

- test04.slx
    PID control loop, variants Arduino hardware/Simulation, TODO: test

- test05.slx(,init.m,inputs.mat)
    Simulation, not Arduino target

    test discrete PID feedback-loop, first-order system and PWM
    input signal designed in Signal Editor and stored in inputs.mat
    initialization in init.m, called in InitFcn callback at "Run" click
