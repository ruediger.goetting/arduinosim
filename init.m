%% Initialization "test05.slx"
%
% Test discrete first-order system, feedback-loop PID, PWM-amplifier
% Attention: Ts < Tpwm < Tp ==> fSample(=100Hz) > fPWM(=10Hz) > fPlant(=1Hz)
%
% Called at "Run" in Simulink model callback function "InitFcn"

Ts=0.01;    % sampling
Tpwm=10*Ts; % pwm
Tp=100*Ts;  % plant
Amp= 25;    % gain of controller
Gd=c2d(tf(12,[Tp,1]),Ts);